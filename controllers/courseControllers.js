
const Course = require("./../models/Course");

module.exports.createCourse = (reqBody) => {

	let newCourse = new Course({
		courseName: reqBody.courseName,
		description: reqBody.description,
		price: reqBody.price

	})

	return newCourse.save().then( (result, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}

module.exports.retrieveCourse = (reqBody) => {

	return Course.find().then( (result, error) => {
		if(error){
			return false
		} else {
			return result
		}
	})
}


module.exports.getActiveCourses = () => {

	return Course.find({isActive:true}).then((result, error) => {
		if(error){
			return error
		} else {
			return result
		}
	})
}

module.exports.getSpecificCourse = (reqBody) => {

	return Course.findOne({courseName: reqBody}).then( (result,error) => {

		if (result == null) {
			return 'Course not existing'
		} else {
			
			if (result){
				return result
			} else {
				return error
			}
		}
	})
}

module.exports.getCourseById = (params) => {


	return Course.findById(params).then( (result,error) => {
		if (result == null) {
			return 'Course not existing'
		} else {
			
			if  (result){
				return result
			} else {
				return error
			}
		}		
	})
}

module.exports.archiveCourse = (reqBody) => {
	let courseStatus = {
		isActive: false
	}

	return Course.findOneandUpdate({courseName: reqBody}, courseStatus, {new: true}).then( (result,error) => {

		if (result == null) {
			return 'Course not existing'
		} else {
			
			if  (result){
				return result
			} else {
				return error
			}
		}	
	})
}

module.exports.unarchiveCourse = (reqBody) => {
	let courseStatus = {
		isActive: true
	}

	return Course.findOneandUpdate({courseName: reqBody}, courseStatus, {new: true}).then( (result,error) => {

		if (result == null) {
			return 'Course not existing'
		} else {
			
			if  (result){
				return result
			} else {
				return error
			}
		}	
	})
}


module.exports.archiveCourseById = (params) => {

	return Course.findByIdAndUpdate(params, {isActive: false}).then( (result,error) => {
		if (result == null) {
			return 'Course not existing'
		} else {
			
			if  (result){
				return true
			} else {
				return false
			}
		}		
	})
}


module.exports.unarchiveCourseById = (params) => {

	return Course.findByIdAndUpdate(params, {isActive: true}).then( (result,error) => {
		if (result == null) {
			return 'Course not existing'
		} else {
			
			if  (result){
				return true
			} else {
				return false
			}
		}		
	})
}


module.exports.deleteCourse = (reqBody) => {

	return Course.findOneandDelete({courseName: reqBody}).then( (result,error) => {
		if (result == null) {
			return 'Course not existing'
		} else {
			
			if  (result){
				return true
			} else {
				return false
			}
		}	
	})
}

module.exports.deleteCourseById = (params) => {

	return Course.findByIdandDelete({params}).then( (result,error) => {
		if (result == null) {
			return 'Course not existing'
		} else {
			
			if  (result){
				return true
			} else {
				return false
			}
		}	
	})
}