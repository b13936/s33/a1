
const express = require("express");
const routes = express.Router();
const userControllers = require("./../controllers/userControllers");
const auth = require("./../auth");


 
routes.get("/email-exists", (req,res) => {

	userControllers.checkEmail(req.body).then( result => res.send(result))
})


routes.post("/register", (req,res) => {

	userControllers.register(req.body).then( result => res.send(result))
})

routes.get("/", (req,res) => {
	userControllers.getAllUsers().then( result => res.send(result))
})

routes.post("/login", (req,res) => {

	userControllers.login(req.body).then(result => res.send(result))
})

routes.get("/details", auth.verify,(req,res) => {

	let userData = auth.decode(req.headers.authorization) 
	console.log(userData)

	userControllers.getProfile(userData).then(result => res.send(result))
})

module.exports = routes;