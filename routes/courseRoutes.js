
const express = require("express");
const router = express.Router();
const courseController = require("./../controllers/courseControllers");
// const auth = require("./../auth");

// create a course
router.post("/create-course", /*auth.verify,*/(req, res) => {

	courseController.createCourse(req.body).then(result => console.log(result));
}) 

router.get("/", (req, res) => {
	
	courseController.retrieveCourse().then(result => console.log(result));
})

router.get("/active-courses", (req, res) => {
	courseController.getActiveCourses().then( result => res.send(result))
})


router.get("/specific-course", (req,res) => {
	courseController.getSpecificCourse(req.body.courseName).then( result => res.send(result))
})

router.get("/:courseId", (req, res) => {

	courseController.getCourseById(req.params.courseId).then(result => res.send(result))
})

router.put("/archive", (req,res) => {

	courseController.archiveCourse(req.body.courseName).then( result => res.send(result))
})
 
router.put("/unarchive", (req,res) => {

	courseController.unarchiveCourse(req.body.courseName).then( result => res.send(result))
})

router.put("/:courseId/archive", (req,res) => {

	courseController.archiveCourseById(req.params.courseId).then( result => res.send(result))
})

router.put("/:courseId/unarchive", (req,res) => {

	courseController.unarchiveCourseById(req.params.courseId).then( result => res.send(result))
})

router.delete("/delete-course", (req,res) => {

	courseController.deleteCourse(req.body.courseName).then(result => res.send(result))
})

router.delete("/:courseId//delete-course", (req,res) => {

	courseController.deleteCourseById(req.params.id).then(result => res.send(result))
})

module.exports = router;