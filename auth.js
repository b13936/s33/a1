
// jsonwebtoken
const jwt = require("jsonwebtoken")
const secret = "CourseBookingAPI";

module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}


	return jwt.sign(data, secret, {});
// .sign(<data>, <secret_key>, {})	
}


module.exports.verify = (req, res, next) => {
	// get the token in the headers authorization
	let token = req.headers.authorization
	// console.log(token)

	token = token.slice(7, token.length)

	// console.log(token)

	if(typeof token !== "undefined"){

		return jwt.verify(token, secret, (err, data) => {

			if(err){
				return res.send( {auth: "failed"})
			}	else {
				next()
			}
		})
	}
}

module.exports.decode = (token) => {

	// console.log(token)

	token = token.slice(7, token.length)
	console.log(token)

	if(typeof token != "undefined"){

		// console.log(jwt.decode(token, {complete: true}))



		return jwt.verify(token, secret, (err,data) => {
			if(err){
				return null
			} else {
				return jwt.decode(token, {complete: true}).payload
			}
		})
		
	}
}

